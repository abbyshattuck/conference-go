from django.http import JsonResponse

from .models import Attendee

from events.models import Conference

from django.views.decorators.http import require_http_methods

import json

from common.json import ModelEncoder


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name", "href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created"]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        response = []
        attendees = Attendee.objects.all()
        for attendee in attendees:
            response.append(
                {
                    "name": attendee.name,
                    "href": attendee.get_api_url(),
                }
            )
        return JsonResponse({"attendees": response})
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_location_pics(city, state):
    params = {"query": city + " " + state, "per_page": 1}
    url = "https://api.pexels.com/v1/search/"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["pictures"][0]["src"]["original"]}
    except:
        return {"Picture_url": None}
